#@title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import tensorflow as tf
from tensorflow import keras
import matplotlib.pyplot as plt
import numpy as np
import TensorFlow.TextClassification.converter as converter

print(tf.__version__)

# Download the IMDB dataset
# The dataset is included with TensorFlow and has already been pre-processed, such that each word is converted to
# indices in a dictionary.
imdb = keras.datasets.imdb
(train_data, train_labels), (test_data, test_labels) = imdb.load_data(num_words=10000)
# The argument num_words=10000 keeps the top 10,000 most frequently occurring words in the training data.
# The rare words are discarded to keep the size of the data manageable.

# Explore the Data
# The data set comes preprocessed: each example is an array of integers representing the words of the movie review.
# Each label is an integer value of either 0 or 1, where 0 is a negative review, and 1 is a positive review.
print("Training entries: {}, labels: {}".format(len(train_data), len(train_labels)))

# The text of reviews have been converted into integers, where each integer represents a specific word in a dictionary.
print(train_data[0])

# Reviews are different lengths. **Inputs to a neural network must be the same length.** This will be resolved later.
print("Review Lengths: train_data[0] -> {}, train_data[1] -> {}".format(len(train_data[0]), len(train_data[1])))

# Decode the review to something readable
print("Decoded Review: {}".format(converter.decode_review(train_data[0])))

# Prepare the Data
# The reviews (arrays of integers) must be converted to tensors before fed into the neural network.
# This conversion can be done a couple of ways, according to the tutorial:
#   1. Convert the arrays into vectors of 0s and 1s indicating word occurrence, similiar to a one-hot encoding
#      [One-Hot Encoding](https://en.wikipedia.org/wiki/One-hot). For example, the sequence [3, 5] would become a
#      10k-dimensional vector that is all zeros except for the indices 3 and 5, which are 1s. This essentially creates
#      boolean state flags. Then, make this the first layer in our network (a Dense layer) that can handle floating
#      point vector data. **This approach is memory intensive**, though, requiring a `num_words * num_reviews` size
#      matrix.
#   2. Alternatively, we can pad the arrays so that they all have the same length, then create an integer tensor of
#      shape `max_length * num_reviews`. We can use an embedded layer capable of handling this shape as the first layer
#      in our network.

# This tutorial uses the second approach, using the pad_sequences function to standardize the lengths.
# Challenge: Once having verifiable output, implement the first approach.
word_index = converter.get_word_index()

train_data = keras.preprocessing.sequence.pad_sequences(train_data,
                                                        value=word_index["<PAD>"],
                                                        padding='post',
                                                        maxlen=256)

test_data = keras.preprocessing.sequence.pad_sequences(test_data,
                                                       value=word_index["<PAD>"],
                                                       padding='post',
                                                       maxlen=256)

print("Review Lengths: train_data[0] -> {}, train_data[1] -> {} (Padded @ 256 max)".format(len(train_data[0]), len(train_data[1])))

# The newly padded first review to compare with before.
# I'm not sure if it's a Python thing that the outputs of the before and after are so different...
print(train_data[0])

# Build the Model
# Creating the model requires two main architectural decisions:
#   1. How many layers to use in the model? (something we've covered in the Basic Classification tutorial)
#   2. How many *hidden units* to use for each layer?

# input shape is the vocabulary count used for the movie reviews (10,000 words)
vocab_size = 10000

model = keras.Sequential()
# It was noted in the documentation for `.Sequential()` that `.add()` was an alternative to using an array when
# calling `.Sequential()`. i.e. `.Sequential([layer1, layer2, etc])`.

# Embedding can only be used as the first layer in a model.
# Turns positive integers (indexes) into dense vectors of fixed size. eg. [[4],[20]] -> [[0.25, 0.1],[0.6, -0.2]]
# The way that a sparse vector differs from a dense vector is that a sparse vector removes values that take up a
# significant amount of space. Considering One-Hot Encoding, this is often 0s and can alleviate a significant amount of
# wasted memory.
# The Embedding layer takes the integer-encoded vocabulary and looks up the embedding vector for each word-index.
# These vectors are learned as the model trains. The vectors add a dimension to the output array.
# The resulting dimensions are: (batch, sequence, embedding).
model.add(keras.layers.Embedding(vocab_size, 16))

# GlobalAveragePooling1D returns a fixed-length output vector for each example by averaging over the sequence dimension.
# This allows the model to handle input of variable length, in the simplest way possible.
model.add(keras.layers.GlobalAveragePooling1D())

# There are 16 hidden units, which are synonymous with nodes or neurons. The more there are, the more complex things
# that can be solved. However, they also risk leaning unwanted behaviors or overfitting.
model.add(keras.layers.Dense(16, activation=tf.nn.relu))

# Using the sigmoid activation function, this value is a float between 0 and 1, representing a probability, or
# confidence level.
model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))

model.summary()

# A model needs a loss function and an optimizer for training. Since this is a binary classification problem and the
# model outputs a probability (a single-unit layer with a sigmoid activation), we'll use the binary_crossentropy loss
# function.

# This isn't the only choice for a loss function, you could, for instance, choose mean_squared_error.
# But, generally, binary_crossentropy is better for dealing with probabilities—it measures the "distance" between
# probability distributions, or in our case, between the ground-truth distribution and the predictions.
model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='binary_crossentropy',
              metrics=['accuracy'])

# Create a Validation Set

# When training, we want to check the accuracy of the model on data it hasn't seen before. Create a validation set by
# setting apart 10,000 examples from the original training data. (Why not use the testing set now? Our goal is to
# develop and tune our model using only the training data, then use the test data just once to evaluate our accuracy).
x_val = train_data[:10000]
partial_x_train = train_data[10000:]

y_val = train_labels[:10000]
partial_y_train = train_labels[10000:]

# Train the Model

# Train the model for 40 epochs in mini-batches of 512 samples.
# This is 40 iterations over all samples in the x_train and y_train tensors.
# While training, monitor the model's loss and accuracy on the 10,000 samples from the validation set.

history = model.fit(partial_x_train,
                    partial_y_train,
                    epochs=40,
                    batch_size=512,
                    validation_data=(x_val, y_val),
                    verbose=1)

# Evaluate the Model
# Two values will be returned. Loss (a number which represents our error, lower values are better), and accuracy.
results = model.evaluate(test_data, test_labels)
print("Results: {}".format(results))
# Apparently, with more advance approaches, the model can get closer to 95% accurate.

# Create a Graph of Accuracy and Loss Over Time
# history, which is returned by `.fit()` above, is an object that contains a dictionary of everything that happened
# during training.
history_dict = history.history
history_dict.keys()

# There are four entries: one for each monitored metric during training and validation. We can use these to plot the
# training and validation loss for comparison, as well as the training and validation accuracy.
acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(1, len(acc) + 1)

# "bo" is for "blue dot"
plt.plot(epochs, loss, 'bo', label='Training loss')
# b is for "solid blue line"
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()

plt.clf()   # clear figure
acc_values = history_dict['acc']
val_acc_values = history_dict['val_acc']

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()

plt.show()
