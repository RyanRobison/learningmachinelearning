#@title MIT License
#
# Copyright (c) 2017 François Chollet
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)

# Loads the data from Google
# Training Images: 60k
# Testing Images: 10k
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# Each image is mapped to a single label.
# Since the class names are not included with the dataset, store them here to use later when plotting the images.
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# The following shows there are 60,000 images in the training set, with each image represented as 28 x 28 pixels
print(train_images.shape)
# Likewise, there are 60,000 labels in the training set
print(len(train_labels))

# Each label is an integer between 0 and 9
print(train_labels)

# There are 10,000 images in the test set. Again, each image is represented as 28 x 28 pixels
print(test_images.shape)
# And the test set contains 10,000 images labels
print(len(test_labels))

# Preprocess the data
# The data must be preprocessed before training the network.
# If you inspect the first image in the training set, you will see that the pixel values fall in the range of 0 to 255.
plt.figure()
plt.imshow(train_images[0])
plt.colorbar()
plt.grid(False)

plt.show()

# Scale the pixel values to a range of 0 to 1 before feeding to the neural network model.
# It's important that the training set and the testing set are preprocessed in the same way.
train_images = train_images / 255.0
test_images = test_images / 255.0

# Display the first 25 images from the training set and display the class name below each image.
# Verify that the data is in the correct format and we're ready to build and train the network.
plt.figure(figsize=(10, 10))
for i in range(25):
    plt.subplot(5, 5, i + 1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    plt.xlabel(class_names[train_labels[i]])

plt.show()

# Build the model

# Setup the layers:
# The basic building block of a neural network is the layer. Layers extract representations from the data fed into them.
# And, hopefully, these representations are more meaningful for the problem at hand.
# Most of deep learning consists of chaining together simple layers.
# Most layers, like tf.keras.layers.Dense, have parameters that are learned during training.

# More information on keras.Sequential [here](https://keras.io/getting-started/sequential-model-guide/)
model = keras.Sequential([
    # The first layer, and only first, determines input "shape" of the model
    # https://www.tensorflow.org/api_docs/python/tf/keras/layers/Flatten
    keras.layers.Flatten(input_shape=(28, 28)),  # All inputs are of this shape
    # transforms the format of the images from a 2d-array (of 28 by 28 pixels), to a 1d-array of 28 * 28 = 784 pixels.
    # Think of this layer as unstacking rows of pixels in the image and lining them up.
    # This layer has no parameters to learn; it only reformats the data.

    # https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense
    # Because the input shape is already defined, it would be redundant to include it in following layers, i.e. Dense
    # These are densely-connected, or fully-connected, neural layers. The first Dense layer has 128 nodes (or neurons).
    # The second (& last) layer is a 10-node softmax layer—this returns an array of 10 probability scores that sum to 1.
    # Each node contains a score that indicates the probability that the current image belongs to one of the 10 classes.
    keras.layers.Dense(128, activation=tf.nn.relu),
    keras.layers.Dense(10, activation=tf.nn.softmax)
])

# Compile the model

# Loss function —This measures how accurate the model is during training.
#   We want to minimize this function to "steer" the model in the right direction.
# Optimizer —This is how the model is updated based on the data it sees and its loss function.
# Metrics —Used to monitor the training and testing steps. The following example uses accuracy, the fraction of the
#   images that are correctly classified.
model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Train the model

# 1. Feed the training data to the model—in this example, the train_images and train_labels arrays.
# 2. The model learns to associate images and labels.
# 3. We ask the model to make predictions about a test set—in this example, the test_images array.
#      We verify that the predictions match the labels from the test_labels array.
model.fit(train_images, train_labels, epochs=5)

# Evaluate Accuracy

test_loss, test_acc = model.evaluate(test_images, test_labels)
print('Test accuracy:', test_acc)
# It turns out, the accuracy on the test dataset is a little less than the accuracy on the training dataset.
# This gap between training accuracy and test accuracy is an example of overfitting.
# Overfitting is when a machine learning model performs worse on new data than on their training data.

# Make Predictions

# Now that the model is trained, we can begin to have the algorithm make predictions about new images
predictions = model.predict(test_images)

print("Predictions:")
print(predictions[0])
# Each of the numbers contained in the output is the machine's "confidence" in each label.
# Notice that there are 10 numbers, which corresponds to the number of labels AND the final Dense layer of the model.

# That max of the confidence metrics (for a given image) is the machine's best guess of the correct category:
print("Most likely category (according to the machine):")
print(np.argmax(predictions[0]))
print(class_names[np.argmax(predictions[0])])


def plot_image(index, predictions_array, true_label, img):
    predictions_array, true_label, img = predictions_array[index], true_label[index], img[index]
    plt.grid(False)  # Don't show the grids on the diagram
    plt.xticks([])  # Getter / Setter - Empty array disables ticks and labels on the diagram
    plt.yticks([])  # Similar to xticks

    plt.imshow(img, cmap=plt.cm.binary)  # Displays an image

    predicted_label = np.argmax(predictions_array)  # Gets max, which is synonymous with the most probable label
    if predicted_label == true_label:
        color = 'blue'  # Visual representation of accuracy
    else:
        color = 'red'  # Visual representation of inaccuracy

    plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label],
                                         100 * np.max(predictions_array),  # "Convert" to a confidence percentage
                                         class_names[true_label]),
               color=color)


def plot_value_array(i, predictions_array, true_label):
    predictions_array, true_label = predictions_array[i], true_label[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    thisplot = plt.bar(range(10), predictions_array, color="#777777")  # Bar plot, specifically
    plt.ylim([0, 1])  # Getter / Setter - sets the limits of the y axis to 0 and 1. Everything should fall between.
    predicted_label = np.argmax(predictions_array)

    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')


i = 0
plt.figure(figsize=(6, 3))
plt.subplot(1, 2, 1)
plot_image(i, predictions, test_labels, test_images)
plt.subplot(1, 2, 2)
plot_value_array(i, predictions, test_labels)

i = 12
plt.figure(figsize=(6, 3))
plt.subplot(1, 2, 1)
plot_image(i, predictions, test_labels, test_images)
plt.subplot(1, 2, 2)
plot_value_array(i, predictions, test_labels)

# Plot the first X test images, their predicted label, and the true label
# Color correct predictions in blue, incorrect predictions in red
num_rows = 5
num_cols = 3
num_images = num_rows * num_cols
plt.figure(figsize=(2 * 2 * num_cols, 2 * num_rows))
for i in range(num_images):
    plt.subplot(num_rows, 2 * num_cols, 2 * i + 1)
    plot_image(i, predictions, test_labels, test_images)
    plt.subplot(num_rows, 2 * num_cols, 2 * i + 2)
    plot_value_array(i, predictions, test_labels)

# Use the trained model to make a prediction about a single image.
# Grab an image from the test dataset
img = test_images[0]
print(img.shape)

# tf.keras models are optimized to make predictions on a batch, or collection, of examples at once.
# So even though we're using a single image, we need to add it to a list.
# Add the image to a batch where it's the only member.
img = (np.expand_dims(img, 0))
print(img.shape)

# Predict the image
predictions_single = model.predict(img)
print(predictions_single)

plt.show()

plot_value_array(0, predictions_single, test_labels)
_ = plt.xticks(range(10), class_names, rotation=45)

plt.show()

# model.predict returns a list of lists, one for each image in the batch of data.
# Grab the predictions for our (only) image in the batch.
print(np.argmax(predictions_single[0]))
