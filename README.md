Learning Machine Learning via the [TensorFlow tutorials](https://www.tensorflow.org/tutorials/).

## Summary of Lessons Learned
#### [Basic Classification](https://gitlab.com/RyanRobison/learningmachinelearning/merge_requests/1)
#### [Text Classification](https://gitlab.com/RyanRobison/learningmachinelearning/merge_requests/2)